package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    // Inspired by Irina Fedortsova, "Creating an Address Book with FXML" @Oracle, 2014
    // just throws the main stage
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("PRHV");
        Pane myPane = FXMLLoader.load(getClass().getResource("prhvBones.fxml"));
        Scene myScene = new Scene(myPane);
        primaryStage.setScene(myScene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

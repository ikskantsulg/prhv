package sample;


import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {

    @FXML
    public TableView<Book> table;
    @FXML
    private TextField isbnField;
    @FXML
    private TextField bookTitleField;
    @FXML
    private TextField authorLastNameField;
    @FXML
    private TextField authorFirstNameField;
    @FXML
    private ChoiceBox status;

    @FXML
    private Button editBtn;
    @FXML
    private Button deleteBtn;
    @FXML
    private Button lendBtn;
    @FXML
    private Button receiveBtn;

    // when addBtn is clicked, the method creates a new Book object using the TextField values it gets
    // & adds the object to an observable list so it could be seen in the TableView
    public void addBook() {
        Book newBook = new Book(
                isbnField.getText(),
                bookTitleField.getText(),
                authorLastNameField.getText(),
                authorFirstNameField.getText(),
                status.getSelectionModel().getSelectedItem().toString()
        );

        ObservableList<Book> data = table.getItems();
        data.add(newBook);
    }

    // when a table row is clicked, all of the other buttons activate
    public void handleRowSelection(){
        if(table.getSelectionModel().getSelectedItem() != null){
            editBtn.setDisable(false);
            deleteBtn.setDisable(false);
            lendBtn.setDisable(false);
            receiveBtn.setDisable(false);
        }
    }


    public void editBook(){
        System.out.println("Book edited");
    }

    public void deleteBook(){
        System.out.println("Book deleted");
    }

    // lendBook & receiveBook are both to check on the row item's status, then allowing or not allowing further action accordingly
    // if the requested action is allowed, the status will be changed and the table refreshed
    public void lendBook(){
        String currentStatus = table.getSelectionModel().getSelectedItem().getStatus();
        if(currentStatus.equals("Pole saadaval")){
            System.out.println("Seda raamatut ei saa välja laenata!");
        } else if(currentStatus.equals("Saadaval")){
            System.out.println("Raamat välja laenatud!");
            table.getSelectionModel().getSelectedItem().setStatus("Pole saadaval");
            table.refresh();
        }
    }

    public void receiveBook(){
        String currentStatus = table.getSelectionModel().getSelectedItem().getStatus();
        if(currentStatus.equals("Saadaval")){
            System.out.println("Seda raamatut ei saa tagastada!");
        } else if(currentStatus.equals("Pole saadaval")){
            System.out.println("Raamat tagastatud!");
            table.getSelectionModel().getSelectedItem().setStatus("Saadaval");
            table.refresh();
        }
    }
}

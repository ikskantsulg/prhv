package sample;

public class Book {
    private String isbnCode;
    private String bookTitle;
    private String authorLastName;
    private String authorFirstName;
    private String status;

    public Book(String isbnCode, String bookTitle, String authorLastName, String authorFirstName, String status) {
        this.isbnCode = isbnCode;
        this.bookTitle = bookTitle;
        this.authorLastName = authorLastName;
        this.authorFirstName = authorFirstName;
        this.status = status;
    }

    public String getIsbnCode() {
        return isbnCode;
    }

    public void setIsbnCode(String isbnCode) {
        this.isbnCode = isbnCode;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

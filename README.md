PRHV - Simple Personal Library Manager

User can add library items (books) to a list, giving them basic characteristics: ISBN, title, author. Once added, the items can be lent out; lent out items - received. 

The application is but a representation of the rookie developer's first steps in the vast, endless world of Java. It most certainly does not apply for absolute completeness. There is still plenty of work to do.

Necessities:
* saving the library items to a file for further use as well
* editing library items
* deleting library items
* checking the input value of the textfields
* throwing dialog boxes with errors if, for example, the input is not suitable -> cannot add an item
* sorting

Would-be-cool-but-not-necessities:
* more characteristics (genre, publisher, amount)
* help section
* borrowers' accounts
* due dates